#include <iostream>

#include <pinger.h>

int main()
{
    Pinger pinger(ADDRESS, PORT);
    pinger.start_ping();
    const std::string &status = pinger.status() ? ONLINE_STR : OFFLINE_STR;

    std::cout << "Content-type: text/html\n\n";
    std::cout << status << std::endl;

    return 0;
}
