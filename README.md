# tes3mp-cgi-pinger

CGI program, that will return status of server on your site.

### How to compile

[Compile raknet](https://gitlab.com/VolkMilit/tes3mp-pinger#compile-crabnet).

```bash
mkdir build && cd build
cmake .. -DRAKNET_INCLUDES="path_to_raknet/includes/raknet" -DRAKNET_LIB="path_to_raknet/build/lib/libRakNetStatic.a"
make
```

You can also specify settings using cmake.

| Setting     | Description                                   |
|-------------|-----------------------------------------------|
| OFFLINE_STR | String, that will print if server is offline. |
| ONLINE_STR  | String, that will print if server is online.  |
| ADDRESS     | Address of the server.                        |
| PORT        | Port of the server.                           |

To use settings, run cmake like this:

```bash
cmake .. -DOFFLINE_STR="Something terrible happens!"
```

### Example of usage, using jQuerry

```js
$.ajax({
	type: "POST",
	url: "/cgi-bin/tes3mp-cgi-pinger.cgi",
	
	success: function(data){
		$('#offline').text(data);
	}
});
```
